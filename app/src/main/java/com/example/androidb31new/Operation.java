package com.example.androidb31new;

public class Operation {
    public static String Calculate(String usersString) {
        String clearString = usersString.replaceAll("\\+|\\-|\\*|\\/", " ");
        String [] parts = clearString.split(" ");
        float first = Float.parseFloat(parts[0]);
        float second = Float.parseFloat(parts[1]);
        String result;
        if (usersString.contains("+")) {
            return result = String.valueOf(first + second);
        } else if (usersString.contains("-")) {
            return result = String.valueOf(first - second);
        } else if (usersString.contains("*")) {
            return result = String.valueOf(first * second);
        } else if (usersString.contains("/")) {
            if (second != 0f){
                return result = String.valueOf(first / second);
            }
            return result = "You can't divide by zero";
        } else {
            return result = "You did smth wrong";
        }
    }
}
